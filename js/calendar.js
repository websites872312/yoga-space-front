(function($) {

	"use strict";

// Setup the calendar with the current date
$(document).ready(init_calendar);

function init_calendar() {
    var date = new Date();
    update_schedule(date);
    // Set click handlers for DOM elements
    $(".right-button").click({date: date}, next_month);
    $(".left-button").click({date: date}, prev_month);
    $(".month").click({date: date}, month_click);
    $("#add-button").click({date: date}, new_event);
    // Set current month as active
    $(".months-row").children().eq(date.getMonth()).addClass("active-month");
}
function update_schedule(date) {
    $.ajax({
      url: "/lessons/"+(date.getYear()+1900).toString()+"/"+date.getMonth().toString()+"/",
      type: "GET",
      dataType: "json",
      success: (data) => {
        event_data = data;
        var lesson_date = date;
        for(var i=0; i<event_data['lessons'].length; i++){
            if(event_data['lessons'][i]['booking']!='completed'){
                var next_lesson = event_data['lessons'][i];
                lesson_date = new Date(next_lesson['year'], next_lesson['month']-1, next_lesson['day']);
                break
            }
        }
        init_schedule(lesson_date);
      },
      error: (error) => {
        console.log(error);
      }
    });
}

// Initialize the calendar by appending the HTML dates
function init_schedule(date) {
    $(".tbody").empty();
    $(".events-container").empty();
    var calendar_days = $(".tbody");
    var month = date.getMonth();
    var year = date.getFullYear();
    var day_count = days_in_month(month, year);
    var row = $("<tr class='table-row'></tr>");
    var today = date.getDate();
    // Set date to 1 to find the first day of the month
    date.setDate(1);
    var first_day = date.getDay();
    // 35+firstDay is the number of date elements to be added to the dates table
    // 35 is from (7 days in a week) * (up to 5 rows of dates in a month)
    for(var i=0; i<35+first_day; i++) {
        // Since some of the elements will be blank, 
        // need to calculate actual date from index
        var day = i-first_day+1;
        // If it is a sunday, make a new row
        if(i%7===0) {
            calendar_days.append(row);
            row = $("<tr class='table-row'></tr>");
        }
        // if current index isn't a day in this month, make it blank
        if(i < first_day || day > day_count) {
            var curr_date = $("<td class='table-date nil'>"+"</td>");
            row.append(curr_date);
        }   
        else {
            var curr_date = $("<td class='table-date'>"+day+"</td>");
            var events = check_events(day, month+1, year);
            if(today===day && $(".active-date").length===0) {
                curr_date.addClass("active-date");
                show_events(events, months[month], day);
            }
            // If this date has any events, style it with .event-date
            if(events.length!==0) {
                curr_date.addClass("event-date");
            }
            const booked_list = ["reserved", "booked"]
            for(var j=0; j<events.length; j++){
                if(booked_list.includes(events[j]["booking"])){
                    curr_date.addClass("event-booked");
                }
            }
            // Set onClick handler for clicking a date
            curr_date.click({events: events, month: months[month], day:day}, date_click);
            row.append(curr_date);
        }
    }
    // Append the last row and set the current year
    calendar_days.append(row);
    $(".year").text(year);
}

// Get the number of days in a given month/year
function days_in_month(month, year) {
    var monthStart = new Date(year, month, 1);
    var monthEnd = new Date(year, month + 1, 1);
    return (monthEnd - monthStart) / (1000 * 60 * 60 * 24);    
}

// Event handler for when a date is clicked
function date_click(event) {
    $(".events-container").show(250);
    $("#dialog").hide(250);
    $(".active-date").removeClass("active-date");
    $(this).addClass("active-date");
    show_events(event.data.events, event.data.month, event.data.day);
};

// Event handler for when a month is clicked
function month_click(event) {
    $(".events-container").show(250);
    $("#dialog").hide(250);
    var date = event.data.date;
    $(".active-month").removeClass("active-month");
    $(this).addClass("active-month");
    var new_month = $(".month").index(this);
    date.setMonth(new_month);
    update_schedule(date);
}

// Event handler for when the month right-button is clicked
function next_month(event) {
    $("#dialog").hide(250);
    var date = event.data.date;
    $(".month").eq(date.getMonth()).removeClass("active-month");
    if (date.getMonth == 11){
        date.setFullYear(date.getFullYear()+1);
        $("year").html(date.getFullYear);
        date.setMonth(0);
    } else {
        date.setMonth(date.getMonth()+1);
    }
    $(".month").eq(date.getMonth()).addClass("active-month");
    update_schedule(date);
}

// Event handler for when the year left-button is clicked
function prev_month(event) {
    $("#dialog").hide(250);
    var date = event.data.date;
    $(".month").eq(date.getMonth()).removeClass("active-month");
    if (date.getMonth == 0){
        date.setFullYear(date.getFullYear()-1);
        $("year").html(date.getFullYear);
        date.setMonth(11);
    } else {
        date.setMonth(date.getMonth()-1);
    }
    $(".month").eq(date.getMonth()).addClass("active-month");
    update_schedule(date);
}

// Event handler for clicking the new event button
function new_event(event) {
    // if a date isn't selected then do nothing
    if($(".active-date").length===0)
        return;
    // remove red error input on click
    $("input").click(function(){
        $(this).removeClass("error-input");
    })
    // empty inputs and hide events
    $("#dialog input[type=text]").val('');
    $("#dialog input[type=number]").val('');
    $(".events-container").hide(250);
    $("#dialog").show(250);
    // Event handler for cancel button
    $("#cancel-button").click(function() {
        $("#name").removeClass("error-input");
        $("#count").removeClass("error-input");
        $("#dialog").hide(250);
        $(".events-container").show(250);
    });
}

// Adds a json event to event_data
function new_event_json(name, count, date, day) {
    var event = {
        "occasion": name,
        "invited_count": count,
        "year": date.getFullYear(),
        "month": date.getMonth()+1,
        "day": day
    };
    event_data["events"].push(event);
}

// Display all events of the selected date in card views
function show_events(events, month, day) {
    // Clear the dates container
    $(".events-container").empty();
    $(".events-container").show(250);
    var d = new Date();
    var time = d.getTime();
    // If there are no events for this date, notify the user
    if(events.length===0) {
        var event_card = $("<div class='event-card'></div>");
        var event_name = $("<div class='event-name'>There are no events planned for "+month+" "+day+".</div>");
        $(event_card).css({ "border-left": "10px solid #FF1744" });
        $(event_card).append(event_name);
        $(".events-container").append(event_card);
    }
    else {
        // Go through and add each event as a card to the events container
        for(var i=0; i<events.length; i++) {
            if(events[i]["state"] == "C") {
                continue;
            }
            var button_value = "Details"
            switch (events[i]["booking"]){
                case "booked":
                    button_value = "Booked";
                    break;
                case "reserved":
                    button_value = "Reserved";
                    break;
                default:
                    button_value = "Details";
                    break;
            }
            var event_card = $(
                "<div class='event-card'>" +
                    "<div class='lesson-details'>" +
                        "<button class='button' id='book-button-"+events[i]["id"]+"'>" +
                            button_value +
                        "</button>" +
                        "<a class='event-name'>" +
                            events[i]["hour"]+":"+String(events[i]["minute"]).padStart(2, '0')+" - "+events[i]["name"]+
                        "</a>" +
                    "</div>" +
                    "<div class='users-booked'>" +
                        "<i class='fa fa-users progress-icon'></i>" +
                        "<div class='progress-back'>" +
                            "<div class='progress-front' style='width:"+parseInt(100*events[i]['nstudents']/events[i]['max_students'])+"%'>" +
                            "</div>" +
                        "</div>" +
                    "</div>" +
                "</div>");
            $(".events-container").append(event_card);

            $("#book-button-"+events[i]["id"]).click({lesson: events[i]}, show_course);
        }
    }
}
// shows the card of a class
function show_course(event){
    var lesson = event["data"]["lesson"];
    $("#course-name").text(lesson["name"] + "   (" + lesson["hour"]+":"+String(lesson["minute"]).padStart(2, '0')+ ")");
    $("#course-image").css("background-image", "url(/static/images/classes/"+lesson["image"]+".webp)");
    $("#course-description").text(lesson["description"]);
//    $("#course-timing").text(lesson["hour"]+":"+String(lesson["minute"]).padStart(2, '0'))
    $("#course-location").text(lesson["location"]);

    switch(lesson["booking"]){
        case "booked":
            $("#book-button").val("Unbook");
            break;
        case "reserved":
            $("#book-button").val("Unreserve");
            break;
        default:
            $("#book-button").val("Book Now!");
            break;
    }
    var date = new Date();
    date.setHours(date.getHours() + 1);
    const lesson_date = new Date(lesson['year']+"/"+lesson['month']+"/"+lesson['day']+" "+lesson['hour']+":"+lesson['minute']+":00");
    if ( date > lesson_date){
        $("#book-button").attr("hidden", true);
    }else{
        $("#book-button").attr("hidden", false);
    }
    $(".events-container").hide(250);
    $("#dialog").show(250);

    // Event handler for cancel button
    $("#cancel-button").click(function() {
        $("#name").removeClass("error-input");
        $("#count").removeClass("error-input");
        $("#dialog").hide(250);
        $(".events-container").show(250);
    });

    // Event handler for book button
    $("#book-button").unbind().click({lesson: lesson}, function() {
        $.ajax({
          url: "/lessons/book/"+lesson.id,
          type: "PUT",
          dataType: "json",
          data: JSON.stringify({'payload': {'booking': lesson["booking"]}}),
          headers: {
            "X-Requested-With": "XMLHttpRequest",
            "X-CSRFToken": getCookie("csrftoken"),
          },
          success: (data) => {
            update_schedule(new Date(lesson.month+'/'+lesson.day+'/'+lesson.year));
            $("#dialog").hide(250);
            $(".events-container").show(250);
          },
          error: (error) => {
            var redirect_url = JSON.parse(error.responseText)['redirect_url']
            if(typeof redirect_url !== "undefined")
            {
              window.location.assign(redirect_url);
            }
          }
        });
    });
}

// Checks if a specific date has any events
function check_events(day, month, year) {
    var events = [];
    for(var i=0; i<event_data["lessons"].length; i++) {
        var event = event_data["lessons"][i];
        if(event["day"]===day &&
            event["month"]===month &&
            event["year"]===year &&
            event["state"]=="A") {
                events.push(event);
            }
    }
    return events;
}

function getCookie(name) {
  let cookieValue = null;
  if (document.cookie && document.cookie !== "") {
    const cookies = document.cookie.split(";");
    for (let i = 0; i < cookies.length; i++) {
      const cookie = cookies[i].trim();
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) === (name + "=")) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}

// Given data for events in JSON format
var event_data = {
    "lessons": [],
    "events": []
};

const months = [ 
    "January", 
    "February", 
    "March", 
    "April", 
    "May", 
    "June", 
    "July", 
    "August", 
    "September", 
    "October", 
    "November", 
    "December" 
];

})(jQuery);
