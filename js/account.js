(function($) {

	"use strict";

// Setup the calendar with the current date
$(document).ready(init_account);

function init_account() {
    get_lessons();
}
function get_lessons() {
    $.ajax({
      url: "/accounts/lessons/",
      type: "GET",
      dataType: "json",
      success: (data) => {
        show_lessons(data["lessons"]);
      },
      error: (error) => {
//        console.log(error);
      }
    });
}

// Display all events of the selected date in card views
function show_lessons(lessons) {
    // Clear the dates container
    var lesson_carousel = $(".wrapper");
    lesson_carousel.empty();
    if(lessons.length===0) {
//        var event_card = $("<div class='event-card'></div>");
//        var event_name = $("<div class='event-name'>There are no events planned for "+month+" "+day+".</div>");
//        $(event_card).css({ "border-left": "10px solid #FF1744" });
//        $(event_card).append(event_name);
//        $(".events-container").append(event_card);
    }
    else {
        lesson_carousel.append($("<h2 style='text-align: center;'>Upcoming Classes</h2>"));
        lesson_carousel.append($("<div class='center-line'><a id='upcoming_reset' class='scroll-icon'><i class='fa fa-caret-up'></i></a></div>"));

        // Event handler for book button
        $("#upcoming_reset").click(function() {
            $("#upcoming_classes").animate({ scrollTop: 0 }, "fast");
        });

        for(var i=0; i<lessons.length; i++) {
            var button_value = "";
            var fa_value = "";
            var cls_booked = "";
            switch (lessons[i]["booking"]){
                case "completed":
                    button_value = completed;
                    fa_value = "fa-backward";
                    cls_booked = "completed";
                    break;
                case "booked":
                    button_value = booked;
                    fa_value = "fa-check";
                    cls_booked = "booked"
                    break;
                case "reserved":
                    button_value = reserved;
                    fa_value = "fa-check";
                    cls_booked = "reserved"
                    break;
                default:
                    if (lessons[i]['nstudents']>=lessons[i]['max_students']){
                        button_value = closed;
                        fa_value = "fa-times";
                        cls_booked = "closed"
                    }else{
                        button_value = open;
                        fa_value = "fa-question";
                    }
                    break;
            }
            var lesson_card = $("<div class='row row-1'></div>");
            var lesson_section = $("<section></section>");
            lesson_section.append($("<i class='icon fa "+fa_value+"'></i>"));
            lesson_section.append($("<div class='details'><span class='title'>"+lessons[i].name+"</span><span>"+lessons[i].day+"-"+lessons[i].month+"-"+lessons[i].year+"</span></div>"));
            lesson_section.append($("<p>"+lessons[i].description+"</p>"));
            lesson_section.append($(
                "<div class='bottom'>"+
                    "<div id='book-"+lessons[i].id+"' value="+i+">"+
                        "<a class='"+cls_booked+"'>"+

//                            "<div class='spinner-border' role='status' style='scale: 0.5;'>"+
//                                "<span class='sr-only'>Loading...</span>"+
//                            "</div>"+
                            button_value+
                        "</a>"+

                    "</div>"+
                    "<a style='background: #ffffff; color: #b3b3b3;' href='"+lessons[i]['maps_link']+"' target='_blank' rel='noopener noreferrer'>"+
                        "<i>- "+
                            lessons[i]['location'].split(':')[0]+
                        "</i>"+
                    "</a>"+
                "</div>"
                ));//
            lesson_card.append(lesson_section);
            lesson_carousel.append(lesson_card);

            if (cls_booked!="completed"){
                // Event handler for book button
                $("#book-"+lessons[i].id).click(function() {
                    var idx = event.target.parentNode.getAttribute("value");
                    if (event.target.textContent == open){
                        event.target.textContent = being_booked;
                    }else{
                        event.target.textContent = being_unbooked;
                    }
                    $.ajax({
                      url: "/lessons/book/"+lessons[idx].id,
                      type: "PUT",
                      dataType: "json",
                      data: JSON.stringify({'payload': {'booking': lessons[idx]["booking"]}}),
                      headers: {
                        "X-Requested-With": "XMLHttpRequest",
                        "X-CSRFToken": getCookie("csrftoken"),
                      },
                      success: (data) => {
                        get_lessons();
                      },
                      error: (error) => {
                        var button = $("#book-"+error.responseJSON['lesson']).children()[0];
                        if (button.classList.contains('reserved')){
                            button.textContent = reserved;
                        } else if (button.classList.contains('booked')){
                            button.textContent = booked;
                        } else {
                            button.textContent = open;
                        }
                      }
                    });
                });
            }
        }
    }
}

function getCookie(name) {
  let cookieValue = null;
  if (document.cookie && document.cookie !== "") {
    const cookies = document.cookie.split(";");
    for (let i = 0; i < cookies.length; i++) {
      const cookie = cookies[i].trim();
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) === (name + "=")) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}
// Given data for events in JSON format
//var lesson_data = [];

var closed = 'Class full'
var open = 'Book Now!'
var being_booked = 'Booking'
var reserved = 'Reserved'
var booked = 'Booked'
var being_unbooked = 'Unbooking'
var completed = 'Finished'
})(jQuery);
